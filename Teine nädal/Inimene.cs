﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teine_nädal
{
    class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>(); // staatiline muutuja, inimeste list, kuhu hakkame inimesi kirja panema
        public string Eesnimi; //väli
        public string Perenimi;
        public DateTime Synniaeg;
        public readonly string IK; //ISIKUKOOD, Readonly asja saab muuta AINULT konstruktori sees
        static int inimesi = 0; //private, inimese järjekorranumber
        int number = ++inimesi; // järjekorranumber
        public static decimal MiinimumPalk = 400; //static on sellepärast ,et seda on AINULT ÜKS kõigile
        private decimal palk = MiinimumPalk; // SEE ON PESA KUS HOITAKSE ALLOLEVA FUNKTSIOONI VÄÄRTUST. Kui jätad võrrandi ära, siis on algväärtus null
        public decimal Palk

        {
            get => palk; //SEE ON FUNKTSIOON, MIS SELLE VÄLJA VÄÄRTUST NÄITAB. 
            set => palk = value > palk ? value : palk; //SEE ON MEETOD, MIS MEIE KONTROLLI ALL LUBAB ANTUD VÄLJALE UUSI VÄÄRTUSI ANDA. palka saab ainult suurendada, mitte vähendada. vähendades palk ei muutu
                                                       //võib ka pikemalt kirjutada
                                                       //                set
                                                       //            {
                                                       //                if (value > palk) palk = value;
                                                       //            }
        }
        public int Vanus //property vanuse arvutamiseks. Siia set'i ei pane, on rread only. Seda ei saa muuta, kuna sünniaeg ongi muutumatu.
        {
            get => (DateTime.Now - Synniaeg).Days * 4 / 1461;
        }
        //KONSTRUKTOR
        //public Inimene() //meetodi nimi on sama, mis on klassi nimi
        //{
        //    Inimesed.Add(this); //this tähendab seda new inimest, kes loodi
        //}
        public Inimene(string ik) //kõikidel inimestel PEAB olema Isikukood. isikukoodi andmine + listi panemine
        {
            this.IK = ik;
            Inimesed.Add(this);
        }
        public Inimene() : this("0000000")// overloadimine ilma parameetrita, ilma IK inimene tehakse ja lisatakse listi, aga talle pannakse automaatselt IK 00000000
        { }
        public override string ToString() => $"{number}. {Eesnimi} (isikukood {IK})";
    }
}
