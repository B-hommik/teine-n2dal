﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teine_nädal
{
    class Program
    {
        static void Main(string[] args)

        {
            Inimene i = new Inimene { Eesnimi = "Thea" };// instantsi väli, läbi muutuja, konkreetne inimene i

            Console.WriteLine(i.Palk);
            Inimene.MiinimumPalk = 800;
            Console.WriteLine((new Inimene { Eesnimi = "Mart" }.Palk)); //static väli, ei pea looma muutujat üldse

            Inimene uus;//muutujal ei ole väärtust
            uus = null;// muutuja väärtus on kandiline null
            //Inimene[rahvas] = new Inimene[5];
            //KOLM VÕIMALUST KUIDAS NIME ANDA
            uus = new Inimene();
            uus.Eesnimi = "Krister";
            uus = new Inimene() { Eesnimi = "Tiit" };//Kustutad Kristeri ära, kui Inimese klassi ei kasuta kõikide nimede listi panemist

            uus = new Inimene { Eesnimi = "Tiiu" };//Kustutad Tiidu ära, kui Inimese klassi ei kasuta kõikide nimede listi panemist

            foreach (var x in Inimene.Inimesed)
            {
                Console.WriteLine(x.Eesnimi);
            }
        }

    }
}
